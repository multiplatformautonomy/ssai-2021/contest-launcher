# Contest Launcher

## Purpose

This repository will be used for running the contest in evaluation mode.  If your autonomy passes a test-run as both player 1 and player 2 in this contest, you should expect it to work well on the contest evaluation servers.

The reason so many docker containers are used here is to sufficiently sandbox all portions of the submitted autonomies.  This will help us maintain the integrity of the contest.
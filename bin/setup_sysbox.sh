#!/bin/bash

# install docker
sudo curl https://get.docker.com | sh
systemctl start docker

# install sysbox
wget https://github.com/nestybox/sysbox/releases/download/v0.2.1/sysbox_0.2.1-0.ubuntu-focal_amd64.deb
docker rm $(docker ps -a -q) -f
sudo apt-get install ./sysbox_0.2.1-0.ubuntu-focal_amd64.deb -y

#!/bin/bash

export DOCKER_BUILDKIT=1

# Docker image provisioning ##############################
# build contest-base
docker build \
    -f /runner/Dockerfiles/contest-base.Dockerfile \
    --tag multiplatformautonomy/ssai-2021/contest-base .

# remove sample contestants and symlink contestant/opponent folders
chmod a+rw /runner/contest-scenario/contestant /runner/contest-scenario/opponent
rm -rf /runner/contest-scenario/contestant /runner/contest-scenario/opponent
mkdir -p /runner/contest-scenario/contestant
ln -s /runner/contest-scenario/contestant /runner/contest-scenario/opponent

# build contest
docker build \
    -f /runner/Dockerfiles/contest.Dockerfile \
    --tag multiplatformautonomy/ssai-2021/contest /runner/contest-scenario

python3 -m venv /runner/venv
source /runner/venv/bin/activate
pip install -r /runner/requirements.txt

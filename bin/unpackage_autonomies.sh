#!/bin/bash

unzip autonomies.zip
ls -lah
rm autonomies.zip
for x in $(ls *.zip | sed -e 's/\.zip$//'); do
    mkdir "p$x"
    mv $x.zip "p$x"
    cd "p$x" && unzip $x.zip && rm $x.zip && cd ..
done

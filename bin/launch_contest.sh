#!/bin/bash

# This file launches a player-vs-player contest simulation run and outputs metrics.
# It takes as an argument an ID for the contest, a zip file, and a contest CSV.
# Contest CSVs should contain 2 columns, player1 and player2, and each row should specify a team ID for each.
# Each zip file should contain python code following the format of what is contained in contestant/sample.
# Of note, it is important that the __init__.py and python files are there.  It is also important that any
# data or model files are present in the zip folder.  See requirements.txt for the allowed packages and versions.
# Python files should be present at the top level of the zip file.

ID=$1
ZIP=$2
CONTEST=$3


if [ -z "$ZIP" ]; then
	echo "Error: Contestant zip file required." 1>&2;
	exit 2
else
	echo "Building contest using contestant code from $ZIP."
fi

docker build --tag multiplatformautonomy/ssai-2021/runner-base -f ./Dockerfiles/runner-base.Dockerfile .
docker build --build-arg CONTESTANT_ZIP=$ZIP --tag multiplatformautonomy/ssai-2021/runner-$ID -f ./Dockerfiles/runner.Dockerfile .

if [ $? -ne 0 ]; then
	echo "Docker failed to build multiplatformautonomy/ssai-2021/runner-$ID." 1>&2
	exit 4;
fi;

for LINE in $(cat $CONTEST); do
    NAME="$ID__$(echo $LINE | sed -e 's/,/_/g')"
    NAME_CODE=$(echo $NAME | md5sum | (head -c 2; tail -c 10))
    PARAM1=$(echo $LINE | cut -d, -f1)
    PARAM2=$(echo $LINE | cut -d, -f2)
    echo "$LINE"
    echo "$PARAM1 $PARAM2"
    docker run --network none \
               --name $NAME \
               --runtime=sysbox-runc \
               multiplatformautonomy/ssai-2021/runner-$ID $PARAM1 $PARAM2
    docker cp $NAME:/runner/outputs/frames.bin ./$NAME.bin
    docker rm $NAME
done

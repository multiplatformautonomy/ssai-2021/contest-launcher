#!/bin/bash

# This runs inside the contest runner container.

PLAYER1="p$1"
PLAYER2="p$2"

if [ -z "$1" ]; then
	echo "Error: please specify exactly 2 players." 1>&2;
	exit 1;
elif [ -z "$2" ]; then
	echo "Error: please specify exactly 2 players." 1>&2;
	exit 2;
fi

source /runner/venv/bin/activate

echo "--- Starting docker daemon inside container..."
dockerd --storage-driver=overlay2 2>&1 >/dev/null & sleep 5

echo "--- Removing old contests..."
# Kill old container
docker kill contest 2>&1 > /dev/null
docker rm contest 2>&1 > /dev/null

echo "Preparing to run contestant $PLAYER1 against contestant $PLAYER2."

ls contestant/$PLAYER1 opponent/$PLAYER2

echo "--- Running new contest..."
# Run new container
docker run --name contest -d -p 6901:6901 -p 6379:6379 multiplatformautonomy/ssai-2021/contest
python3 /runner/contest-scenario/contest.py -c $PLAYER1 -o $PLAYER2 -e 1 -t

FROM multiplatformautonomy/ssai-2021/runner-base
ARG CONTESTANT_ZIP

COPY ./bin/unpackage_autonomies.sh /runner/unpackage_autonomies.sh
COPY --chown=runner ${CONTESTANT_ZIP} /runner/contest-scenario/contestant/autonomies.zip
RUN /runner/unpackage_autonomies.sh
COPY ./bin/contest_run.sh /runner/contest-scenario/contest_run.sh
RUN chown -R runner:runner /runner && chmod -R 0700 /runner
WORKDIR /runner/contest-scenario
ENTRYPOINT ["./contest_run.sh"]

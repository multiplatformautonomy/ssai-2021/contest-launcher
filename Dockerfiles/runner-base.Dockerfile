FROM python:3.9.2-slim-buster

RUN apt update && apt install -y curl wget bash zip

# Install docker
RUN curl https://get.docker.com | sh
# WARNING: Always run this image from sysbox container.
# This installs docker within docker more securely.

RUN adduser --home /runner --disabled-password runner && \
    usermod -aG docker runner
WORKDIR /runner

# Copy Files
ADD Dockerfiles /runner/Dockerfiles
ADD contest-scenario /runner/contest-scenario
COPY ./requirements.txt /runner/requirements.txt
COPY ./bin/provision.sh /runner/provision.sh
RUN dockerd & /runner/provision.sh

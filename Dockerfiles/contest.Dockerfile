FROM multiplatformautonomy/contest-base

COPY . /server

WORKDIR /server
RUN mkdir build && cd build && cmake .. && make && make install && chmod +x /server/contestserver.py

EXPOSE 6379

CMD source ~/.scrimmage/setup.bash && \
    echo requirepass "${REDIS_PASSWORD}" >> ~/redis.conf && \
    (redis-server ~/redis.conf & /server/contestserver.py)
